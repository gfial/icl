package ast;

import java.util.List;

import types.BoolType;
import types.Type;
import types.TypeErrorException;
import utils.Environment;
import utils.Environment.DuplicatedIdentifierException;
import utils.Environment.UndeclaredIdentifierException;
import values.BoolValue;
import values.NumValue;
import values.Value;

import compiler.JvmFrame;
import compiler.JvmClass;
import compiler.JvmCodeBlock;

public class ASTEqual implements ASTNode {

	private ASTNode left, right;
	private Type type;

	public Value eval(Environment<Value> env) throws UndeclaredIdentifierException, DuplicatedIdentifierException {
		return new BoolValue(((NumValue) left.eval(env)).value == ((NumValue) right.eval(env)).value);
	}

	public ASTEqual(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public void compile(JvmCodeBlock c, List<JvmClass> classes, JvmFrame env) {
		String l1 = c.getNextLabel();
		String l2 = c.getNextLabel();
		
		left.compile(c, classes, env);
		right.compile(c, classes, env);
		c.emit_if_equal(l1);
		c.emit_push(0);
		c.emit_goto(l2);
		c.emit_label(l1);
		c.emit_push(1);
		c.emit_label(l2);
		
	}

	@Override
	public Type typeCheck(Environment<Type> env)
			throws TypeErrorException, UndeclaredIdentifierException, DuplicatedIdentifierException {
		
		Type t1, t2;
		
		t1 = left.typeCheck(env);
		t2 = right.typeCheck(env);
		
		if(t1 == t2) {
			return type = BoolType.value;
		}
		
		else
			throw new TypeErrorException(t1 + " value cannot be compared to a value of type " + t2 + ".");
	}

	@Override
	public Type getType() {
		return type;
	}

}

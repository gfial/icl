package ast;

import java.util.List;

import compiler.JvmClass;
import compiler.JvmCodeBlock;
import compiler.JvmFrame;
import types.Type;
import types.TypeErrorException;
import utils.Environment;
import utils.Environment.DuplicatedIdentifierException;
import utils.Environment.UndeclaredIdentifierException;
import values.Value;

public class ASTSemicolon implements ASTNode {

	ASTNode left, right;
	Type typeValue;
	
	public ASTSemicolon(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}
	
	@Override
	public Value eval(Environment<Value> env) throws UndeclaredIdentifierException, DuplicatedIdentifierException {
		left.eval(env);
		return right.eval(env);
	}

	@Override
	public void compile(JvmCodeBlock c, List<JvmClass> classes, JvmFrame frame) {
		left.compile(c, classes, frame);
		right.compile(c, classes, frame);
	}

	@Override
	public Type typeCheck(Environment<Type> env)
			throws TypeErrorException, UndeclaredIdentifierException, DuplicatedIdentifierException {
		left.typeCheck(env);
		return typeValue = right.typeCheck(env);
	}

	@Override
	public Type getType() {
		
		return typeValue;
	}

}

package ast;

import java.util.List;

import compiler.JvmClass;
import compiler.JvmCodeBlock;
import compiler.JvmFrame;
import compiler.JvmIntVar;
import compiler.JvmObjVar;
import compiler.JvmTypes;
import compiler.JvmUtils;
import types.BoolType;
import types.IntType;
import types.RefType;
import types.Type;
import types.TypeErrorException;
import utils.Environment;
import utils.Environment.DuplicatedIdentifierException;
import utils.Environment.UndeclaredIdentifierException;
import values.RefValue;
import values.Value;

public class ASTAssign implements ASTNode {

	private ASTNode left, right;
	private Type valueType;
	
	public ASTAssign(ASTNode left, ASTNode right)
	{
		this.left = left; this.right = right;
	}
	
	public Value eval(Environment<Value> env) throws UndeclaredIdentifierException, DuplicatedIdentifierException {
		RefValue ref = (RefValue) left.eval(env);
		Value val = right.eval(env);
		ref.setValue(val);
		return val; 
	}

    @Override
    public String toString() {
    	return left.toString() + " := " + right.toString();
    }

	@Override
	public Type typeCheck(Environment<Type> env) throws UndeclaredIdentifierException, DuplicatedIdentifierException, TypeErrorException {

		RefType reftype = (RefType) left.typeCheck(env);
		Type type = right.typeCheck(env);
		if( reftype.getType().equals(type)) {
			this.valueType = type;
			return type;
		}
		else
			throw new TypeErrorException("Type mismatch on assign.");
	}

	@Override
	public void compile(JvmCodeBlock c, List<JvmClass> classes, JvmFrame frame) {		
		
		Type refType = right.getType();
		
		left.compile(c, classes, frame);
		right.compile(c, classes, frame);
		
		if(refType instanceof IntType || refType instanceof BoolType)
			c.emit_putfield(JvmIntVar.CLASSNAME, JvmClass.DEFAULT_VAR_NAME, JvmTypes.getIntegerType());
		else
			c.emit_putfield(JvmObjVar.CLASSNAME, JvmClass.DEFAULT_VAR_NAME, JvmUtils.typeToJvm(refType));
	}

	@Override
	public Type getType() {
		return valueType;
	}
	
}

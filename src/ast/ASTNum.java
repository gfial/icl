package ast;

import java.util.List;

import types.IntType;
import types.Type;
import utils.Environment;
import values.NumValue;
import values.Value;

import compiler.JvmFrame;
import compiler.JvmClass;
import compiler.JvmCodeBlock;

public class ASTNum implements ASTNode {

	private int val;
	private Type valueType;

	public Value eval(Environment<Value> env) {
		return new NumValue(val);
	}

	public ASTNum(int n) {
		val = n;
	}

	@Override
	public void compile(JvmCodeBlock c, List<JvmClass> classes, JvmFrame env) {
		
		c.emit_push(val);
	}

	@Override
	public Type typeCheck(Environment<Type> env) {
		
		return valueType = IntType.value;
	}

	@Override
	public Type getType() {
		return valueType;
	}

}


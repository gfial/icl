package ast;

import java.util.Iterator;
import java.util.List;

import compiler.JvmFrame;
import compiler.JvmClass;
import compiler.JvmCodeBlock;
import compiler.JvmUtils;
import types.Type;
import types.TypeErrorException;
import utils.Association;
import utils.Environment;
import utils.Environment.DuplicatedIdentifierException;
import utils.Environment.UndeclaredIdentifierException;
import values.Value;

public class ASTDecl implements ASTNode {

	private static int framec = 0;
	private List<Association<ASTNode>> assocs;
	private ASTNode body;
	private Type type;
	
	public ASTDecl(List<Association<ASTNode>> assocs, ASTNode body) {
		this.assocs = assocs;
		this.body = body;
	}
	
	@Override
	public Value eval(Environment<Value> env) throws UndeclaredIdentifierException, DuplicatedIdentifierException {
		
		Environment<Value> newEnv = env.beginScope();
		Iterator<Association<ASTNode>> it = assocs.iterator();
		
		while(it.hasNext()) {
			
			Association<ASTNode> next = it.next();
			newEnv.assoc(next.id, next.value.eval(newEnv));
		}
		
		return body.eval(newEnv);
	}

	@Override
	public void compile(JvmCodeBlock c, List<JvmClass> classes, JvmFrame frame) {
		
		// Generate new JVM class for this specific DECL
		JvmFrame newFrame = new JvmFrame("f" + framec++, frame);
		classes.add(newFrame);
		
		c.emit_new(newFrame.getName());
		c.emit_dup();
		c.emit_invokespecial(newFrame.getName());
		
		for(Association<ASTNode> assoc: assocs) {
			c.emit_dup();
			assoc.value.compile(c, classes, newFrame);
			newFrame.putField(assoc.id, JvmUtils.typeToJvm(assoc.value.getType()));
			c.emit_putfield(newFrame.getName(), assoc.id, JvmUtils.typeToJvm(assoc.value.getType()));
		}
		
		c.emit_astore(1);
		body.compile(c, classes, newFrame);
	}

	@Override
	public Type typeCheck(Environment<Type> env)
			throws TypeErrorException, UndeclaredIdentifierException, DuplicatedIdentifierException {
		
		Environment<Type> newEnv = env.beginScope();
		Iterator<Association<ASTNode>> it = assocs.iterator();
		
		while(it.hasNext()) {
			
			Association<ASTNode> next = it.next();
			newEnv.assoc(next.id, next.value.typeCheck(newEnv));
		}
		
		return type = body.typeCheck(newEnv);
	}

	@Override
	public Type getType() {
		return type;
	}

}

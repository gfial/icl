package ast;

import java.util.List;

import compiler.JvmClass;
import compiler.JvmCodeBlock;
import compiler.JvmFrame;
import compiler.JvmIntVar;
import compiler.JvmObjVar;
import compiler.JvmTypes;
import types.BoolType;
import types.IntType;
import types.RefType;
import types.Type;
import types.TypeErrorException;
import utils.Environment;
import utils.Environment.DuplicatedIdentifierException;
import utils.Environment.UndeclaredIdentifierException;
import values.RefValue;
import values.Value;

public class ASTVar implements ASTNode {
	
	private ASTNode child;
	private Type valueType;
	
	@Override
	public Value eval(Environment<Value> env)
			throws UndeclaredIdentifierException, DuplicatedIdentifierException {
		Value val = child.eval(env);
		return new RefValue(val);
	}
	
	public ASTVar(ASTNode c) {
		child=c;
	}
	
	public String toString() {
	    	return "var( " + child.toString() + ")";
	}
	
	@Override
	public void compile(JvmCodeBlock c, List<JvmClass> classes, JvmFrame frame) {
		
		JvmClass varClass = null;
		String jvmtype = null;
		
		if(child.getType() instanceof IntType || child.getType() instanceof BoolType) {
			varClass = new JvmIntVar();
			jvmtype = JvmTypes.getIntegerType();
		}
		
		else if(child.getType() instanceof RefType) {
			varClass = new JvmObjVar();
			jvmtype = JvmTypes.getLinkType("Java/Lang/Object");
		}
		
		if(!classes.contains(varClass))
			classes.add(varClass);
		
		c.emit_new(varClass.getName());
		c.emit_dup();
		c.emit_invokespecial(varClass.getName());
		c.emit_dup();
		child.compile(c, classes, frame);
		c.emit_putfield(varClass.getName(), JvmClass.DEFAULT_VAR_NAME, jvmtype);
		
	}

	@Override
	public Type typeCheck(Environment<Type> env) throws TypeErrorException,
			UndeclaredIdentifierException, DuplicatedIdentifierException {
		
		Type t = child.typeCheck(env);
		return valueType = new RefType(t);
	}

	@Override
	public Type getType() {
		return valueType;
	}

}

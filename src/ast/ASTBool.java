package ast;

import java.util.List;

import types.BoolType;
import types.Type;
import types.TypeErrorException;
import utils.Environment;
import utils.Environment.DuplicatedIdentifierException;
import utils.Environment.UndeclaredIdentifierException;
import values.BoolValue;
import values.Value;

import compiler.JvmFrame;
import compiler.JvmClass;
import compiler.JvmCodeBlock;

public class ASTBool implements ASTNode {

	private boolean value;
	private Type type;
	
	public Value eval(Environment<Value> env) {
		return new BoolValue(value);
	}

	public ASTBool(boolean val) {
		value = val;
	}

	@Override
	public void compile(JvmCodeBlock c, List<JvmClass> classes, JvmFrame env) {
		c.emit_push(value ? 1 : 0);
	}

	@Override
	public Type typeCheck(Environment<Type> env)
			throws TypeErrorException, UndeclaredIdentifierException, DuplicatedIdentifierException {
		
		return type = BoolType.value;
	}

	@Override
	public Type getType() {

		return type;
	}

}

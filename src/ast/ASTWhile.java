package ast;

import java.util.List;

import types.BoolType;
import types.Type;
import types.TypeErrorException;
import utils.Environment;
import utils.Environment.DuplicatedIdentifierException;
import utils.Environment.UndeclaredIdentifierException;
import values.BoolValue;
import values.Value;

import compiler.JvmFrame;
import compiler.JvmClass;
import compiler.JvmCodeBlock;

public class ASTWhile implements ASTNode{

	private ASTNode whileterm, doterm;
	
	public ASTWhile( ASTNode whileterm, ASTNode doterm){
		this.whileterm = whileterm;
		this.doterm = doterm;
	}
	
	@Override
	public Value eval(Environment<Value> env)
			throws UndeclaredIdentifierException, DuplicatedIdentifierException {
		
		while(((BoolValue)whileterm.eval(env)).value) {
			doterm.eval(env);
		}
		
		return new BoolValue(false);
	}

	@Override
	public void compile(JvmCodeBlock c, List<JvmClass> classes, JvmFrame frame) {
		
		String whileLabel = c.getNextLabel();
		String endLabel = c.getNextLabel();
		
		c.emit_label(whileLabel);
		whileterm.compile(c, classes, frame);
		c.emit_push(1);
		c.emit_if_nequal(endLabel);
		doterm.compile(c, classes, frame);
		c.emit_goto(whileLabel);
		c.emit_label(endLabel);
		
	}

	@Override
	public Type typeCheck(Environment<Type> env) throws TypeErrorException,
			UndeclaredIdentifierException, DuplicatedIdentifierException {
		
		Type t = whileterm.typeCheck(env);
		
		doterm.typeCheck(env);
		
		if(t instanceof BoolType){
			return BoolType.value;
		}else
			throw new TypeErrorException( t + " is not a boolean.");
	}

	@Override
	public Type getType() {
		return BoolType.value;
	}

}

package ast;

import java.util.List;

import types.IntType;
import types.Type;
import types.TypeErrorException;
import utils.Environment;
import utils.Environment.DuplicatedIdentifierException;
import utils.Environment.UndeclaredIdentifierException;
import values.NumValue;
import values.Value;

import compiler.JvmFrame;
import compiler.JvmClass;
import compiler.JvmCodeBlock;

public class ASTNeg implements ASTNode {

	private ASTNode expr;
	private Type type;

	public ASTNeg(ASTNode left)
	{
		this.expr = left;
	}

	public Value eval(Environment<Value> env) throws UndeclaredIdentifierException, DuplicatedIdentifierException { 
		return new NumValue(- ((NumValue) expr.eval(env)).value); 
	}

    @Override
    public String toString() {
    	return "- " + expr.toString() ;
    }

	@Override
	public Type typeCheck(Environment<Type> env)
			throws UndeclaredIdentifierException, DuplicatedIdentifierException, TypeErrorException {

		Type t1 = expr.typeCheck(env);
		
		if( t1 == IntType.value  ) 
			return type = IntType.value;
		else 
			throw new TypeErrorException("Using a non-number where a number was expected.");
		
	}

	@Override
	public void compile(JvmCodeBlock c, List<JvmClass> classes, JvmFrame env) {
		expr.compile(c, classes, env);
		c.emit_neg();
	}

	@Override
	public Type getType() {
		return type;
	}

}

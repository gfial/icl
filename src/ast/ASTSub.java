package ast;

import java.util.List;

import types.IntType;
import types.Type;
import types.TypeErrorException;
import utils.Environment;
import utils.Environment.DuplicatedIdentifierException;
import utils.Environment.UndeclaredIdentifierException;
import values.NumValue;
import values.Value;

import compiler.JvmFrame;
import compiler.JvmClass;
import compiler.JvmCodeBlock;

public class ASTSub implements ASTNode {

	private ASTNode left, right;
	private Type valueType;

	public Value eval(Environment<Value> env) throws UndeclaredIdentifierException, DuplicatedIdentifierException {
		return new NumValue(((NumValue)left.eval(env)).value - ((NumValue)right.eval(env)).value);
	}

	public ASTSub(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}
	
	@Override
	public void compile(JvmCodeBlock c, List<JvmClass> classes, JvmFrame env) {
		left.compile(c, classes, env);
		right.compile(c, classes, env);
		c.emit_sub();
	}

	@Override
	public Type typeCheck(Environment<Type> env)
			throws TypeErrorException, UndeclaredIdentifierException, DuplicatedIdentifierException {
		
		Type t1, t2;
		
		t1 = left.typeCheck(env);
		t2 = right.typeCheck(env);
		
		if(t1 == IntType.value && t2 == IntType.value) {
			return valueType = IntType.value;
		}
		
		else
			throw new TypeErrorException( t1 + " value cannot be subtracted to a value of type " + t2 + ".");
	}

	@Override
	public Type getType() {
		return valueType;
	}

}


package ast;

import java.util.List;

import types.BoolType;
import types.Type;
import types.TypeErrorException;
import utils.Environment;
import utils.Environment.DuplicatedIdentifierException;
import utils.Environment.UndeclaredIdentifierException;
import values.BoolValue;
import values.Value;

import compiler.JvmFrame;
import compiler.JvmClass;
import compiler.JvmCodeBlock;

public class ASTOr implements ASTNode {

	private ASTNode left, right;
	private Type valueType;

	public Value eval(Environment<Value> env) throws UndeclaredIdentifierException, DuplicatedIdentifierException {
		return new BoolValue(((BoolValue)left.eval(env)).value || ((BoolValue)right.eval(env)).value);
	}

	public ASTOr(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public void compile(JvmCodeBlock c, List<JvmClass> classes, JvmFrame env) {
		left.compile(c, classes, env);
		right.compile(c, classes, env);
		c.emit_or();
	}

	@Override
	public Type typeCheck(Environment<Type> env)
			throws TypeErrorException, UndeclaredIdentifierException, DuplicatedIdentifierException {

		Type t1, t2;
		
		t1 = left.typeCheck(env);
		t2 = right.typeCheck(env);
		
		if(t1 == BoolType.value && t2 == BoolType.value) {
			return BoolType.value;
		}
		
		else
			throw new TypeErrorException(t1 + " value cannot be compared to a value of type " + t2 + ".");
	}

	@Override
	public Type getType() {
		return valueType;
	}

}


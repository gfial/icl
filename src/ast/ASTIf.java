package ast;

import java.util.List;

import types.BoolType;
import types.Type;
import types.TypeErrorException;
import utils.Environment;
import utils.Environment.DuplicatedIdentifierException;
import utils.Environment.UndeclaredIdentifierException;
import values.BoolValue;
import values.Value;

import compiler.JvmClass;
import compiler.JvmCodeBlock;
import compiler.JvmFrame;


public class ASTIf implements ASTNode {

	private ASTNode ift, thent, elset;
	private Type type;
	
	public ASTIf(ASTNode ifterm, ASTNode thenterm, ASTNode elseterm) {
		
		ift = ifterm;
		thent = thenterm;
		elset = elseterm;
		
	}
	
	@Override
	public Value eval(Environment<Value> env) throws UndeclaredIdentifierException, DuplicatedIdentifierException {
		
		BoolValue ifvalue = (BoolValue)ift.eval(env);
		
		if(ifvalue.value) {
			return (thent.eval(env));
		}
		
		return elset.eval(env);
	}

	@Override
	public void compile(JvmCodeBlock c, List<JvmClass> classes, JvmFrame frame) {

		String elseLabel = c.getNextLabel();
		String endLabel = c.getNextLabel();

		ift.compile(c, classes, frame);
		c.emit_push(1);
		c.emit_if_nequal(elseLabel);
		thent.compile(c, classes, frame);
		c.emit_goto(endLabel);
		c.emit_label(elseLabel);
		elset.compile(c, classes, frame);
		c.emit_label(endLabel);

	}

	@Override
	public Type typeCheck(Environment<Type> env)
			throws TypeErrorException, UndeclaredIdentifierException, DuplicatedIdentifierException {
		
		if(!(ift.typeCheck(env) instanceof BoolType))
			throw new TypeErrorException("<IF> condition must have a " + BoolType.value + " expression inside");
		
		Type thenType = thent.typeCheck(env);
		if(thenType.equals(elset.typeCheck(env)))
			return type = thenType;
		
		throw new TypeErrorException("<IF> condition does not have same result types in the <THEN> and <ELSE> clauses.");
	}

	@Override
	public Type getType() {
		return type;
	}
}

package ast;

import java.util.List;

import compiler.JvmFrame;
import compiler.JvmClass;
import compiler.JvmCodeBlock;
import types.Type;
import types.TypeErrorException;
import utils.Environment;
import utils.Environment.DuplicatedIdentifierException;
import utils.Environment.UndeclaredIdentifierException;
import values.Value;

public interface ASTNode {

	/**
	 * Provides the value that is associated to the node
	 * @return The node value
	 */
	Value eval(Environment<Value> env) throws UndeclaredIdentifierException, DuplicatedIdentifierException;

	/**
	 * Compiles the AST into JVM code.
	 * @param c - JVM code block to be dumped into a file (main JVM class).
	 * @param classes - List of JVM classes to be created.
	 * @param env - JVM current frame class, works similarly to Environment<br>
	 * from eval() or typeCheck()
	 */
    void compile(JvmCodeBlock c, List<JvmClass> classes, JvmFrame env);
    
    /**
     * Verifies if there are no type errors on the existing AST.
     * @param env - current Environment.
     * @return The type of this ASTNode.
     * @throws TypeErrorException If a type error has been found.
     * @throws UndeclaredIdentifierException If there is an undeclared identifier.
     * @throws DuplicatedIdentifierException If more than one identifier share the same name.
     */
    Type typeCheck(Environment<Type> env) throws TypeErrorException, UndeclaredIdentifierException, DuplicatedIdentifierException;
    
    /**
     * Returns the type of this ASTNode.
     * @pre Should only be called after typechecker.
     * @return The type of the ASTNode.
     */
    Type getType();
}


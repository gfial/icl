package ast;

import java.util.List;

import types.Type;
import types.TypeErrorException;
import utils.Environment;
import utils.Environment.DuplicatedIdentifierException;
import utils.Environment.UndeclaredIdentifierException;
import values.Value;

import compiler.JvmClass;
import compiler.JvmCodeBlock;
import compiler.JvmField;
import compiler.JvmFrame;

public class ASTId implements ASTNode {

	public final String id;
	private Type valueType;

	public Value eval(Environment<Value> env) throws UndeclaredIdentifierException {
		return env.find(id);
	}

	public ASTId(String id) {
		this.id = id;
	}

	@Override
	public void compile(JvmCodeBlock c, List<JvmClass> classes, JvmFrame currentFrame) {

		JvmFrame frame = currentFrame;
		JvmField field;
		
		c.emit_aload(1);
		c.emit_checkcast(frame.getName());
		field = frame.getField(id);
		
		while(field == null) {
			JvmFrame temp = frame;
			frame = frame.getPreviousLevel();
			c.emit_getfield(temp.getName(), JvmFrame.SL, "L" + frame.getName() + ";");
			field = frame.getField(id);
		}
		
		c.emit_getfield(frame.getName(), id, field.type);
			
	}

	@Override
	public Type typeCheck(Environment<Type> env)
			throws TypeErrorException, UndeclaredIdentifierException, DuplicatedIdentifierException {
		
		return valueType = env.find(id);
	}

	@Override
	public Type getType() {
		return valueType;
	}

}


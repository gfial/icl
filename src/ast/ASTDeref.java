package ast;

import java.util.List;

import types.BoolType;
import types.IntType;
import types.RefType;
import types.Type;
import types.TypeErrorException;
import utils.Environment;
import utils.Environment.DuplicatedIdentifierException;
import utils.Environment.UndeclaredIdentifierException;
import values.RefValue;
import values.Value;

import compiler.JvmFrame;
import compiler.JvmIntVar;
import compiler.JvmObjVar;
import compiler.JvmTypes;
import compiler.JvmUtils;
import compiler.JvmClass;
import compiler.JvmCodeBlock;

public class ASTDeref implements ASTNode {
	
	private ASTNode child;
	private Type typeValue;
	
	public ASTDeref(ASTNode child){
		
		this.child = child;
	}
	
	@Override
	public Value eval(Environment<Value> env)
			throws UndeclaredIdentifierException, DuplicatedIdentifierException {
		RefValue ref = (RefValue) child.eval(env);
		return ref.getValue();
	}

	public String toString() {
    	return "*" + child.toString();
    }
    
	@Override
	public void compile(JvmCodeBlock c, List<JvmClass> classes, JvmFrame frame) {
		
		Type refType = ((RefType)child.getType()).getType();
		
		child.compile(c, classes, frame);
		
		if(refType instanceof IntType || refType instanceof BoolType)
			c.emit_getfield(JvmIntVar.CLASSNAME, JvmClass.DEFAULT_VAR_NAME, JvmTypes.getIntegerType());
		else
			c.emit_getfield(JvmObjVar.CLASSNAME, JvmClass.DEFAULT_VAR_NAME, JvmUtils.typeToJvm(refType));
	}

	@Override
	public Type typeCheck(Environment<Type> env) throws TypeErrorException,
			UndeclaredIdentifierException, DuplicatedIdentifierException {
		
		Type t = child.typeCheck(env);
		
		if(t instanceof RefType){
			return typeValue = ((RefType) t).getType();
		}
		else 
			throw new TypeErrorException("Trying to deref a non-reference value");
	}

	@Override
	public Type getType() {
		return typeValue;
	}

}

package values;

public class BoolValue implements Value {

public final Boolean value;
	
	public BoolValue(boolean value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return value.toString();
	}
	
}

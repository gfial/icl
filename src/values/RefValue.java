package values;

public class RefValue implements Value {
	Value val;
	
	public RefValue(Value val){
		this.val = val;
		
	}
	
	public Value getValue(){
		return val;
	}
	
	public void setValue(Value val){
		this.val=val;
	}
	
	public String toString(){
		return "Ref("+val.toString()+")";
	}
	
	public boolean equals (Object other){
		return this == other;
	}
}

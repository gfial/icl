package values;

public class NumValue implements Value {

	public final Integer value;
	
	public NumValue(int value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return value.toString();
	}
	
}

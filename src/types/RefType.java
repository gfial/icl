package types;

public class RefType implements Type{

	public Type type;
	
	public RefType(Type type){
		this.type=type;
	}
	
	public Type getType(){
		return type;
	}
	
	
	public String toString() {
		return "ref(" + type + ")";
	}
	
	public boolean equals(Object other){
		if(other instanceof RefType){
			Type t = ((RefType) other).getType();
			return this.type.equals(t);
		}else
			return false;
			
	}
}

package types;

public class TypeErrorException extends Exception {

	private static final long serialVersionUID = 1L;
	private String message;
	
	public TypeErrorException(String message) {
		
		this.message = message;
	}
	
	public String getError() {
		return message;
	}
	
}
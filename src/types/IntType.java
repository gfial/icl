package types;

public class IntType implements Type {

	public final static IntType value = new IntType();
	
	Type value() {
		return value;
	}
	
	public String toString() {
		return "Integer";
	}
	
}

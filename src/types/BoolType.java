package types;

public class BoolType implements Type {

	public final static BoolType value = new BoolType();

	Type value() {
		return value;
	}

	public String toString() {
		return "Boolean";
	}

}

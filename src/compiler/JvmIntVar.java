package compiler;

import java.io.PrintStream;

/**
 * This class represents a JVM class that contains a referenced Integer.<br>
 * @author guilherme
 *
 */
public class JvmIntVar implements JvmClass {
	
	private JvmField var;
	public static final String CLASSNAME = "IntegerVar";
	
	public JvmIntVar() {
		var = new JvmField(DEFAULT_VAR_NAME, JvmTypes.getIntegerType());
	}
	
	@Override
	public String getName() {
		return CLASSNAME;
	}

	@Override
	public void print(PrintStream out) {
		
		out.println(".class " + CLASSNAME);
		out.println(".super java/lang/Object");
		var.print(out);
		out.println(".method public <init>()V");
		out.println("aload_0");
		out.println("invokespecial java/lang/Object/<init>()V");
		out.println("return");
		out.println(".end method");

	}

}

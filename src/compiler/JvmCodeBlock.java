package compiler;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class JvmCodeBlock {
	
	List<String> code;
	int labelc;

	public JvmCodeBlock() {
		labelc = 0;
		code = new ArrayList<>();
	}

	public String getNextLabel() {
		return "l" + labelc++;
	}

	public void emit_new(String className) {
		code.add("new " + className);
	}
	
	/**
	 * Duplicates the value on top of the stack.
	 */
	public void emit_dup() {
		code.add("dup");
	}
	
	/**
	 * Loads the value stored in the given position onto the stack;
	 * @param position - memory position to load from
	 */
	public void emit_aload(int position) {
		code.add("aload " + position);
	}
	
	/**
	 * Consumes the value on top of the stack and stores it in a given<br>
	 * memory position;
	 * @param position - memory position where to store the value
	 */
	public void emit_astore(int position) {
		code.add("astore " + position);
	}
	
	public void emit_putfield(String className, String varName, String type) {
		code.add("putfield " + className + "/" + varName + " " + type);
	}
	
	public void emit_getfield(String className, String varName, String type) {
		code.add("getfield " + className + "/" + varName + " " + type);
	}
	
	/**
	 * Checks that the top item on the operand stack <br>
	 * (a reference to an object or array) can be cast to a given type;
	 * @param className - item name
	 */
	public void emit_checkcast(String className) {
		code.add("checkcast " + className);
	}
	
	/**
	 * Cleans the top of the stack;
	 */
	public void emit_aconst_null() {
		code.add("aconst_null");
	}
	
	public void emit_() {
		code.add("");
	}
	
	/**
	 * Calls the object constructor;
	 * @param objName - name of the Object
	 */
	public void emit_invokespecial(String objName) {
		code.add("invokespecial " + objName + "/<init>()V");
	}
	
	/**
	 * Places a label <i>label</i> where to jump to<br>
	 * and execute the instructions that follow.
	 * @param label is the label name;
	 */
	public void emit_label(String label) {
		code.add(label + ":");
	}

	/**
	 * Jumps to a given <i>label</i>.
	 * @param label is the name of the label where to<br>
	 * jump to;
	 */
	public void emit_goto(String label) {
		code.add("goto " + label);
	}

	/**
	 * Pushes an Integer onto the stack.
	 * 
	 * @param n is the Integer to be pushed
	 */
	public void emit_push(int n) {
		code.add("sipush " + n);
	}

	/**
	 * Pops the two values at the top of the stack, adds<br>
	 * them and pushes the result back onto the stack
	 */
	public void emit_add() {
		code.add("iadd");
	}

	/**
	 * Pops the two values at the top of the stack, subtracts<br>
	 * the top one from the second one and then pushes the<br>
	 * result back onto the stack
	 */
	public void emit_sub() {
		code.add("isub");
	}
	
	public void emit_neg(){
		code.add("ineg");
	}

	/**
	 * Pops the top two integers from the operand stack and divides<br>
	 * the second-from top integer (value2) by the top integer (value1),<br>
	 * i.e. computes (value2 / value1).<br>
	 * The quotient result is truncated to the nearest integer<br>
	 * (with rounding going towards zero, so 1.7 becomes 1)<br>
	 * and placed on the stack
	 */
	public void emit_div() {
		code.add("idiv");
	}

	/**
	 * Pops the two values at the top of the stack, multiplies<br>
	 * them and pushes the result back onto the stack
	 */
	public void emit_mul() {
		code.add("imul");
	}

	/**
	 * Pops the two values at the top of the stack, computes the<br>
	 * bitwise OR and pushes the result back onto the stack
	 */
	public void emit_or() {
		code.add("ior");
	}

	/**
	 * Pops the two values at the top of the stack, computes the<br>
	 * bitwise AND and pushes the result back onto the stack
	 */
	public void emit_and() {
		code.add("iand");
	}

	/**
	 * Pops the top two ints off the stack and compares them.<br>
	 * If the two values are equal, execution branches to<br>
	 * the given label.<br>
	 * <p>If the two values are not equal execution continues at<br>
	 * the next instruction.</p>
	 * @param label is the label where to jump if the<br>
	 * condition is verified.
	 */
	public void emit_if_equal(String label) {
		code.add("if_icmpeq " + label);
	}
	
	/**
	 * Pops the top two ints off the stack and compares them.<br>
	 * If the two values are not equal, execution branches to<br>
	 * the given label.<br>
	 * <p>If the two values are equal execution continues at<br>
	 * the next instruction.</p>
	 * @param label is the label where to jump if the<br>
	 * condition is verified.
	 */
	public void emit_if_nequal(String label) {
		code.add("if_icmpneq " + label);
	}
	
	/**
	 * Pops the top two ints off the stack and compares them.<br>
	 * If value2 is greater than value1, execution branches to<br>
	 * the given label.<br>
	 * <p>If value2 is less than or equal to value1,<br>
	 * execution continues at the next instruction.</p>
	 * @param label is the label where to jump if the<br>
	 * condition is verified.
	 */
	public void emit_if_gt(String label) {
		code.add("if_icmpgt " + label);
	}
	
	/**
	 * Pops the top two ints off the stack and compares them.<br>
	 * If value2 is less than value1, execution branches to<br>
	 * the given label.<br>
	 * <p>If value2 is greater than or equal to value1,<br>
	 * execution continues at the next instruction.</p>
	 * @param label is the label where to jump if the<br>
	 * condition is verified.
	 */
	public void emit_if_lt(String label) {
		code.add("if_icmplt " + label);
	}

	public void print(PrintStream out) {
		for (String line: code)
			out.println(line);
	}
}

package compiler;

import java.io.PrintStream;

public interface JvmClass {
	
	public static final String DEFAULT_VAR_NAME = "loc";
	
	public String getName();
	
	public void print(PrintStream out);
	
}

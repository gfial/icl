package compiler;

import types.BoolType;
import types.IntType;
import types.RefType;
import types.Type;

public class JvmUtils {

	public static String typeToJvm(Type type) {
		
		if(type instanceof IntType || type instanceof BoolType) {
			return JvmTypes.getIntegerType();
		}
		
		if(type instanceof RefType) {
			Type ref = ((RefType) type).getType();
			if(ref instanceof RefType)
				return JvmTypes.getLinkType(JvmObjVar.CLASSNAME);
			else if(ref instanceof IntType || ref instanceof BoolType)
				return JvmTypes.getLinkType(JvmIntVar.CLASSNAME);
		}
			
		return null;
	}
	
}

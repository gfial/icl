package compiler;

import java.io.PrintStream;

public class JvmField {

	public final String name, type;
	
	public JvmField(String name, String type) {
		this.name = name;
		this.type = type;
	}
	
	public void print(PrintStream out) {
		
		out.println(".field public " + name + " " + type);
	}

}

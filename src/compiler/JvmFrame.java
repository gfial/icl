package compiler;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

public class JvmFrame implements JvmClass {
	
	public static final String SL = "_SL";
	private String frameName;
	private JvmFrame previousLevel;
	private Map<String, JvmField> fields;

	public JvmFrame(String name, JvmFrame frame) {
		previousLevel = frame;
		frameName = name;
		fields = new HashMap<String, JvmField>();
		
		if(frame != null)
			putField( SL, "L" + frame.getName() + ";");
	}
	
	/**
	 * Obtains the previous JvmFrame.
	 * @return The previous JvmFrame.
	 */
	public JvmFrame getPreviousLevel() {
		return previousLevel;
	}
	
	/**
	 * Returns the field identified by the given name.
	 * @param name - The field name.
	 * @return The field (as a JvmField object).
	 */
	public JvmField getField(String name) {
		
		return fields.get(name);
	}

	public void putField(String name, String type) {
		
		fields.put(name, new JvmField(name, type));
	}

	@Override
	public String getName() {
		return frameName;
	}

	@Override
	public void print(PrintStream out) {
		
		out.println(".class " + frameName);
		out.println(".super java/lang/Object");
		//out.println(".implements frame");
		
		for(JvmField field: fields.values())
			field.print(out);
		
		out.println(".method public <init>()V");
		out.println("aload_0");
		out.println("invokespecial java/lang/Object/<init>()V");
		out.println("return");
		out.println(".end method");
	}
}

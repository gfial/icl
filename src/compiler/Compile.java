package compiler;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

import parser.ParseException;
import parser.ParserLogic;
import parser.TokenMgrError;
import types.Type;
import types.TypeErrorException;
import utils.Environment;
import utils.Environment.DuplicatedIdentifierException;
import utils.Environment.UndeclaredIdentifierException;
import ast.ASTNode;

public class Compile {

	public static void main(String[] args) {

		new ParserLogic(System.in);
		JvmCodeBlock c = new JvmCodeBlock();
		JvmMain main = new JvmMain("main", c);
		List<JvmClass> classes = new LinkedList<JvmClass>();
		classes.add(main);
		ASTNode exp;

		while (true) {
			try {
				exp = ParserLogic.Start();
				exp.typeCheck(new Environment<Type>());
				exp.compile(c, classes, null);
				
				for (JvmClass jvmClass: classes) {
					File f = new File(jvmClass.getName() + ".j");
					PrintStream out = new PrintStream(f);
					jvmClass.print(out);
				}
			} catch (TokenMgrError | ParseException e) {
				System.out.println("Syntax error: " + e.getMessage());
				ParserLogic.ReInit(System.in);
			} catch (UndeclaredIdentifierException e) {
				System.out.println("Identifier not found: " + e.getId());
			} catch (DuplicatedIdentifierException e) {
				System.out.println("Duplicated identifier: " + e.getId());
			} catch (TypeErrorException e) {
				System.out.println("Type error: " + e.getError());
			} catch (IOException e) {
				System.out.println("Problem handling a file: " + e.getMessage());
			}
		}
	}

}

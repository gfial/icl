package compiler;

import java.io.PrintStream;

public class JvmMain implements JvmClass {

	private JvmCodeBlock code;
	private String name;
	
	public JvmMain(String name, JvmCodeBlock c) {
		this.name = name;
		code = c;
	}
	
	@Override
	public void print(PrintStream out) {
		out.println(".class public " + name);
		out.println(".super java/lang/Object");
		out.println(".method public <init>()V");
		out.println("aload_0");
		out.println("invokenonvirtual java/lang/Object/<init>()V");
		out.println("return");
		out.println(".end method");
		out.println(".method public static main([Ljava/lang/String;)V");
		out.println(".limit locals 256");
		out.println(".limit stack 256");
		out.println("getstatic java/lang/System/out Ljava/io/PrintStream;");
		
		code.print(out);
		
		out.println("invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
		out.println("invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		out.println("return");
		out.println(".end method");

	}

	@Override
	public String getName() {
		return name;
	}
	
}

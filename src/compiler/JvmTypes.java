package compiler;

/**
 * Provides the JVM types.
 * @author guilherme
 *
 */
public class JvmTypes {

	public static String getIntegerType() {
		return "I";
	}
	
	public static String getLinkType(String obj) {
		return "L" + obj + ";";
	}
}

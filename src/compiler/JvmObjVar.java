package compiler;

import java.io.PrintStream;

/**
 * This class represents a JVM class that contains a referenced<br>
 * JVM Class/Object.
 * @author guilherme
 *
 */
public class JvmObjVar implements JvmClass {

	private JvmField var;
	public static final String CLASSNAME = "ObjectVar";
	
	public JvmObjVar() {
		var = new JvmField(DEFAULT_VAR_NAME, JvmTypes.getLinkType("java/lang/Object"));
	}
	
	@Override
	public String getName() {
		return CLASSNAME;
	}

	@Override
	public void print(PrintStream out) {
		
		out.println(".class " + CLASSNAME);
		out.println(".super java/lang/Object");
		var.print(out);
		out.println(".method public <init>()V");
		out.println("aload_0");
		out.println("invokespecial java/lang/Object/<init>()V");
		out.println("return");
		out.println(".end method");

	}

}

package interpreter;

import parser.ParseException;
import parser.ParserLogic;
import parser.TokenMgrError;
import ast.ASTNode;
import types.Type;
import types.TypeErrorException;
import utils.Environment;
import utils.Environment.DuplicatedIdentifierException;
import utils.Environment.UndeclaredIdentifierException;
import values.Value;

public class Interpreter {

	/** Main entry point. */
	public static void main(String args[]) {
		new ParserLogic(System.in);
		ASTNode exp;

		while (true) {
			try {
				exp = ParserLogic.Start();
				exp.typeCheck(new Environment<Type>());
				System.out.println(exp.eval(new Environment<Value>()));
			} catch (TokenMgrError | ParseException e) {
				System.out.println("Syntax error: " + e.getMessage());
				ParserLogic.ReInit(System.in);
			} catch (UndeclaredIdentifierException e) {
				System.out.println("Identifier not found: " + e.getId());
			} catch (DuplicatedIdentifierException e) {
				System.out.println("Duplicated identifier: " + e.getId());
			} catch (TypeErrorException e) {
				System.out.println("Type error: " + e.getError());
			}
		}
	}
}
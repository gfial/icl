package utils;

public class Association<T> {

	public final String id;
	public final T value;
	
	public Association(String id, T value) {
		
		this.id = id;
		this.value = value;
	}
	
}

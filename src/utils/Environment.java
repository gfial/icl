package utils;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Environment<T> {

	public static class UndeclaredIdentifierException extends Exception {

		private static final long serialVersionUID = 1L;
		private String id;
		
		public UndeclaredIdentifierException(String id) {
			
			this.id = id;
		}
		
		public String getId() {
			return id;
		}
		
	}
	
	public static class DuplicatedIdentifierException extends Exception {

		private static final long serialVersionUID = 1L;
		private String id;
		
		public DuplicatedIdentifierException(String id) {
			
			this.id = id;
		}
		
		public String getId() {
			return id;
		}
		
	}
	
	private Environment<T> previousLevel;
	private List<Association<T>> assocs;
	
	public Environment() {
		this(null);
	}
	
	public Environment(Environment<T> previous) {
		previousLevel = previous;
		assocs = new LinkedList<Association<T>>();
	}
	
	public Environment<T> beginScope() {
		return new Environment<T>(this);
	}
	
	public Environment<T> endScope() {
		return previousLevel;
	}
	
	/**
	 * Recursively searches the identifier through environments.
	 * @param id The identifier.
	 * @return The identifier value.
	 * @throws UndeclaredIdentifierException If such identifier was not found.
	 */
	public T find(String id) throws UndeclaredIdentifierException {
		
		T value = findInCurrentLevel(id);
		
		if(value == null) {
			if(previousLevel == null)
				throw new UndeclaredIdentifierException(id);
			
			value = previousLevel.find(id);
		}
		
		return value;
	}
	
	/**
	 * 
	 * @param id
	 * @param value
	 * @throws DuplicatedIdentifierException
	 */
	public void assoc(String id, T value) throws DuplicatedIdentifierException {
		
		if(findInCurrentLevel(id) != null)
			throw new DuplicatedIdentifierException(id);
		
		assocs.add(new Association<T>(id, value));
	}
	
	private T findInCurrentLevel(String id) {
		
		Iterator<Association<T>> it = assocs.iterator();
		Association<T> current = null;
		while(it.hasNext()) {
			
			current = it.next();
			if(current.id.equals(id)) {
				return current.value;
			}

		}
		return null;
	}
	
}
